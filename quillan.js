(function($) {
  $(document).ready(function(){
    
    for (var key in Drupal.settings.Quillans){
      var quillan = Drupal.settings.Quillans[key];
      var source ;      
      $(quillan.elementid).each(function() {
        source = $(this);
      });
      source.hide();
      source.after(quillan.image);
      $(quillan.elementid).click(function(e) {
        e.preventDefault();
      });      
      $('#'+quillan.quillan_element).click(function() {        
        $(this).hide('explode', {pieces: 16}, 1000);
        source.unbind('click');
        setTimeout(function(){
          source.show();
          $.ajax({
            type: 'POST',
            url: Drupal.settings.quillanupdateurl,
            dataType: 'json',
            success: processResponse,
            data: 'qid='+ quillan.qid
          });
        }, 1000);        
      });
    };
  });

  var processResponse = function (data) {
    if (data.redirect == true) {
      window.location = data.url;
    }
    else {
      window.location.reload();
    }
  }

})(jQuery);
